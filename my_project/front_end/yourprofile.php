<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>
<?php include_once 'header.php'; ?>
<div class = "container" id = "prof_pic">
     <?php 
     $uid = $_SESSION['user_session'];
     $display -> viewpicture($uid);
     ?>
     <a href = "addpic.php">Change Picture</a>
     <p><?php echo $display->getUsername($uid) ?></p>
</div>
<div class = "container" id = "prof_desc">
     <?php
     $uid = $_SESSION['user_session'];
     $display -> viewdesc($uid);
     ?>
     <br/>
     <a href = "modifydesc.php">Change Description</a>
</div>

<?php include_once('footer.php');?>
