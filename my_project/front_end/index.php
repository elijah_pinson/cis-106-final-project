<?php
session_start();
require('database/dbconfig.php');
require_once("database/class.user.php");
$login = new USER();

if($login->is_loggedin()!="")
{
	$login->redirect('homepage.php');
}

if(isset($_POST['btn-login']))
{
	$uname = strip_tags($_POST['txt_uname_email']);
	$umail = strip_tags($_POST['txt_uname_email']);
	$upass = strip_tags($_POST['txt_password']);
		
	if($login->doLogin($uname,$umail,$upass))
	{
		$login->redirect('homepage.php');
	}
	else
	{
		$error = "Wrong Details !";
	}	
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ashley Walruson</title>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen"> 
<link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css"  />
</head>

<body>

<div class="signin-form">

	<div class="container">
       <form class="form-signin" method="post" id="login-form">
           <span class = "title">Ashley Walruson</span>
           <div id = "walruses">
                <img src = "https://upload.wikimedia.org/wikipedia/commons/6/65/Walrus_-_Kamogawa_Seaworld_-_2.jpg" alt = "Find a walrus to love" />
            </div>
        <h2 class="form-signin-heading">Login</h2><hr />
        
        <div id="error">
        <?php
			if(isset($error))
			{
				?>
                <div class="alert alert-danger">
                    &nbsp; <?php echo $error; ?> 
                </div>
                <?php
			}
		?>
        </div>
        
        <div class="form-group">
        <input type="text" class="form-control" name="txt_uname_email" placeholder="Username or E mail ID" required />
        <span id="check-e"></span>
        </div>
        
        <div class="form-group">
        <input type="password" class="form-control" name="txt_password" placeholder="Your Password" />
        </div>
       
     	<hr />
        
        <div class="form-group">
            <button type="submit" name="btn-login" class="btn btn-default">
                &nbsp; SIGN IN
            </button>
        </div>  
      	<br />
            <label>Don't have an account yet? <a href="sign-up.php">Sign Up!</a></label>
            <br/>
            <a href = "about.php">About</a>
            <br/>
            <a href = "contact.php">Contact Us</a>
      </form>

    </div>
    
</div>
<?php require_once('footer.php');