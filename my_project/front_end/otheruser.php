<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>

<?php
    $uid = $_GET['id'];
    if ($uid === $_SESSION['user_session'])
    {
        header("Location: yourprofile.php");
    }
?>
<?php include_once 'header.php'; ?>
<div class = "container" id = "prof_pic">
     <?php 
     $display -> viewpicture($uid);
     ?>
     <p><?php echo $display->getUsername($uid) ?></p>
     <a href = "messagesender.php?toid=<?php echo $uid;?>">Send Message?</a>
</div>
<div class = "container" id = "prof_desc">
     <?php
     $display -> viewdesc($uid);
     ?>
     <br/>

</div>

<?php include_once('footer.php');?>
