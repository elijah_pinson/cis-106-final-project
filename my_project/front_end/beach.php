<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>
<?php include_once('header.php');?>

<div class="container" id ="beach">
	<h2 class ="table_head">Welcome to the Beach!</h2>
	<div class = "container" id ="beach_table">
	<table class='table table-bordered table-responsive'>
		<?php
			$userid = $_SESSION['user_session'];
			$display->beachview($userid);
		?>
	</table>
	</div>
</div>



<?php include_once('footer.php');?>