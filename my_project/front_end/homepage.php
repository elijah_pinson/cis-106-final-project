<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>
<?php include_once 'header.php'; ?>
		
			<div class="container" id ="new_users">
				<h2 class ="table_head">New Users</h2>
				<div class = "container" id ="users_table">
				<table class='table table-bordered table-responsive'>
					<?php
						$query = "SELECT user_name, date_joined, user_id FROM users ORDER BY YEAR(date_joined) DESC, MONTH(date_joined) DESC, DAY(date_joined) DESC";       
						$display->newusers_view($query);
					?>
				</table>
				</div>
			</div>
			
			
			<div class="container" id ="news">
				<h2 id ="table_head">News</h2>
				<div class = "container" id ="news_table">
				<table class='table table-bordered table-responsive'>
					<?php
						$query = "SELECT news_title, news_link FROM news LIMIT 10";       
						$display->news_view($query);
					?>
				</table>
				</div>
			</div>
		</div>
<?php include_once('footer.php');?>

