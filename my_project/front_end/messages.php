<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>
<?php include_once 'header.php'; ?>
		
			<div class="container" id = "tomessages">
				<h2 class ="table_head">Sent</h2>
				<div class = "container" id ="message_table">
				<table class='table table-bordered table-responsive'>
					<?php
						$display->messages_view("sent");
					?>
				</table>
				</div>
			</div>
			
			
			<div class="container" id = "frommessages">
				<h2 id ="table_head">Received</h2>
				<div class = "container" id ="message_table">
				<table class='table table-bordered table-responsive'>
					<?php
  
						$display->messages_view("received");
					?>
				</table>
				</div>
			</div>
		</div>
<?php include_once('footer.php');?>

