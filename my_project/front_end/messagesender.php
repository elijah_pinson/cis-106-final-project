<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.crud.php';
include_once 'database/class.display.php';
$crud = new crud();
$display = new display();

if(isset($_POST['btn-save']))
{
    $touser = $_GET['toid'];
	$umessage = $_POST['message'];

    if ($crud->sendmessage($umessage, $touser))
    {
        header("Location: messages.php");
    }
    else
    {
        header("Location: messagesender.php?failure");
    }


	
}


if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>ERROR!</strong> Failed to send!
	</div>
	</div>
    <?php
}
?>
<?php include_once 'header.php'; ?>


<div class="container" id = "tomessages">
	<form method='post'>
 
    <table class='table table-bordered' id = "message_table">
 
        <tr>
            <td><?php if (isset($_GET['toid'])) 
                        { 
                            $to_name = $display ->getUsername($_GET['toid']);
                            echo $to_name;
                        } 
                        else 
                        {
                            echo "no username specified";
                        }?></td>
        </tr>
 
        <tr>
             <td><textarea rows="4" cols="50" name="message"></textarea> </td>
        </tr>
 
 
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
    		<span class="glyphicon glyphicon-plus"></span> Send Message
			</button>  
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>

<?php include_once 'footer.php'; ?>