<?php

class crud
{
	
	private $db;
	
	public function __construct()
	{
		$database = new Database();
		$dbConnection = $database->dbConnection();
		$this->db = $dbConnection;
    }
	
	
	public function getID($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM users WHERE user_id=:id");
		$stmt->execute(array(":id"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}
	
	
	
	
	public function addpicture($imgFile, $tmp_dir, $imgSize) 
	{
		error_reporting( ~E_NOTICE );
		if(empty($imgFile))
		{
			$errMSG = "Please Select Image File.";
			return false;
		}
		else
		{	
			$id = $_SESSION['user_session'];
			$upload_dir = 'user_images/'; // upload directory
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			// valid image extensions
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			// rename uploading image
			$userpic = rand(1000,1000000).".".$imgExt;
			// allow valid image file formats
			if(in_array($imgExt, $valid_extensions))
			{   
				// Check file size '5MB'
				if($imgSize < 5000000)    
				{
					$stmt_select = $this->db->prepare('SELECT user_picture FROM users WHERE user_id =:id');
					$stmt_select->bindParam(':id', $id);
					$stmt_select->execute(array(':id'=>$id));;
					$imgRow=$stmt_select->fetch(PDO::FETCH_ASSOC);
					unlink("user_images/".$imgRow['user_picture']);
					
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
					
					$stmt = $this->db->prepare("UPDATE users SET user_picture = :userpic WHERE user_id = :id");
					$stmt->bindParam(':userpic', $userpic);
					$stmt->bindParam(':id', $id);
					$stmt -> execute();
					return true;
				}
				else
				{
					$errMSG = "Sorry, your file is too large.";
					return false;
				}
			}
			else
			{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";  
				return false;
			}
		}
	}
	
	public function modify_description($udesc)
	{
		try
		{
			$uid = $_SESSION['user_session'];
			$stmt = $this->db->prepare("UPDATE users SET user_description = :udesc WHERE user_id = :uid");
			$stmt->bindParam(":udesc",$udesc);
			$stmt->bindParam(":uid", $uid);
			$stmt->execute();
			return true;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
		
	}
	
	public function sendmessage($message, $recipient)
	{
		try
		{
			$uid = $_SESSION['user_session'];
			$stmt = $this->db->prepare("INSERT INTO messages (to_user, from_user, content) VALUES (:recipient, :uid, :message)");
			$stmt->bindParam(":recipient",$recipient);
			$stmt->bindParam(":uid", $uid);
			$stmt->bindParam(":message",$message);
			$stmt->execute();
			return true;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}

	
}


