<?php

class display
{
	
	private $db;
	
	public function __construct()
	{
		$database = new Database();
		$dbConnection = $database->dbConnection();
		$this->db = $dbConnection;
    }
	
	public function getUsername($id)
	{
		$stmt = $this->db->prepare("SELECT user_name FROM users WHERE user_id=:id");
		$stmt->execute(array(":id"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow['user_name'];
	}
	
	public function newusers_view($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	
		if($stmt->rowCount()>0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				$id = $row['user_id'];
				?>
                <tr>
                <td><a href = "otheruser.php?id=<?php echo $id;?>"><?php print($row['user_name']); ?></a></td>
                <td><?php print("joined the herd on "); print($row['date_joined']); ?></td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}
		
	}
	
	public function news_view($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	
		if($stmt->rowCount()>0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				$news = $row['news_link'];
				?>
                <tr>
                	<?php print($row['news_title']); ?><br/>
                	<a target = '_blank' href = "<?php echo $news; ?>"><?php print($row['news_link']); ?></a></br>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}
		
	}
	
	public function viewpicture($uid)
	{
		$query = ("SELECT user_picture FROM users WHERE user_id = :uid");
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':uid', $uid);
		$stmt->execute();
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		if(!empty($row['user_picture']))
		{
			
			extract($row);
			?>
				<img src="user_images/<?php echo $row['user_picture']; ?>" class="img-rounded"/>
	        <?php
		}
		else
		{
			?>
            <img src = "user_images/default.jpg" class="img-rounded" />
            <?php
		}
		
	}

	public function viewdesc($uid)
	{
		$query = ("SELECT user_description FROM users WHERE user_id = :uid");
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(':uid', $uid);
		$stmt->execute();
		$row=$stmt->fetch(PDO::FETCH_ASSOC);
		if(!empty($row['user_description']))
		{
			extract($row);
			echo $row['user_description'];

		}
		else {
			?>
			<p>No Description</p>
			<?php
		}

	}
	
	public function messages_view($direction)
	{
		if ($direction === "sent")
		{
			$userid = $_SESSION['user_session'];
			$stmt = $this->db->prepare("SELECT * FROM messages WHERE from_user = :userid ORDER BY id DESC");
			$stmt->bindParam(":userid", $userid);
			$stmt->execute();
			if($stmt->rowCount()>0)
			{
				while($row=$stmt->fetch(PDO::FETCH_ASSOC))
				{
					$message = $row['content'];
					$to = $row['to_user'];
					$to_name = $this->getUsername($to);
					?>
		       	 	<tr>
		        		<td><a href = "otheruser.php?id=<?php echo $to;?>"><?php print($to_name); ?></a></td>
		        		<td><?php echo $message?></td>
		        	</tr>
        			<?php
				}
			}
			else
			{
				?>
            	<tr>
            	<td>No Messages</td>
            	</tr>
            	<?php
			}
		}
		else if ($direction === "received")
		{
			$userid = $_SESSION['user_session'];
			$stmt = $this->db->prepare("SELECT * FROM messages WHERE to_user = :userid ORDER BY id DESC");
			$stmt->bindParam(":userid", $userid);
			$stmt->execute();
			if($stmt->rowCount()>0)
			{
				while($row=$stmt->fetch(PDO::FETCH_ASSOC))
				{
					$message = $row['content'];
					$from = $row['from_user'];
					$from_name = $this->getUsername($from);
					?>
		       	 	<tr>
	        			<td><a href = "otheruser.php?id=<?php echo $from;?>"><?php print($from_name); ?></a></td>
	        			<td><?php echo $message?></td>
	        			<td><a href = "messagesender.php?toid=<?php echo $from;?>">Reply?</a></td>
	        		</tr>
        			<?php
				}
			}
			else
			{
				?>
            	<tr>
            	<td>No Messages</td>
            	</tr>
            	<?php
			}
		}
	}
	
	public function beachview($userid)
	{
		$query = ("SELECT user_name, user_picture, user_id FROM users WHERE user_id != :userid");
		$stmt = $this->db->prepare($query);
		$stmt->bindParam(":userid",$userid);
		$stmt->execute();
		if($stmt->rowCount()>0)
		{
			?>
			<tr>
			<?php
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				?>
            		<td><?php $this->viewpicture($row['user_id']);?>
            		<br/><a href = "otheruser.php?id=<?php echo $row['user_id'];?>"><?php print($row['user_name']); ?></a></td>
            	<?php

			}
			?></tr><?php
		}
		else 
		{
			echo "no data";	
		}
	}
	/* JSON */
	
	public function jsonview($query){
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		$json = array();
	
		if($stmt->rowCount()>0) 
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				$json[] = $row;
			}
		}
		
		return json_encode($json);
		
	}
	
	
}


