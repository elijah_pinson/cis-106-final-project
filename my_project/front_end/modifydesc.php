<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.crud.php';
$crud = new crud();

if(isset($_POST['btn-save']))
{
	$udesc = $_POST['user_description'];
    if ($crud->modify_description($udesc))
    {
        header("Location: yourprofile.php");
    }
    else
    {
        header("Location: modifydesc.php?failure");
    }


	
}
?>



<?php

if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>ERROR!</strong> Requirements not met
	</div>
	</div>
    <?php
}
?>

<?php include_once 'header.php'; ?>

<div class="container">
	<form method='post'>
        <textarea rows="4" cols="50" name="user_description"></textarea> 
        <br>
        <button type="submit" class="btn btn-primary" name="btn-save">
    	    <span class="glyphicon glyphicon-plus"></span> Change Description
    	</button>  
    </form>
     
</div>

<?php include_once 'footer.php'; ?>