<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.crud.php';
$crud = new crud();

if(isset($_POST['btn-save']))
		{
		
			$imgFile = $_FILES['user_picture']['name'];
			$tmp_dir = $_FILES['user_picture']['tmp_name'];
			$imgSize = $_FILES['user_picture']['size'];
			
  if($crud->addpicture($imgFile, $tmp_dir, $imgSize))
	{
		header("Location: yourprofile.php");
	}
	else
	{
		header("Location: addpic.php?failure");
	
	
	
}
?>

<? php
if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>ERROR!</strong> Failed to upload! 
	</div>
	</div>
    <?php
}
?>
<?php include_once 'header.php'; ?>
<div class = "container" id = "addpic">
	<form method='post' enctype = "multipart/form-data">
    
    <table class='table table-bordered table-responsive' id = "pic_table">
 
      <tr>
         <td><label class="control-label">Profile Picture</label></td>
        <td><input class= "input-group" type="file" name="user_picture" accept="image/*" required /></td>
     </tr>
 
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
    		<span class="glyphicon glyphicon-plus"></span> &nbsp; Upload Image
			</button>  
            </td>
        </tr>
 
    </table>
</form>
</div>
<?php include_once('footer.php');?>