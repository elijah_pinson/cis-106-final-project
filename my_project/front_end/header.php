<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ashley Walruson</title>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen"> 
<link href="css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="css/style.css" type="text/css"  />
</head>

<body>
<!-- Static navbar -->
    <div class="container">
        <div class="row">
            <h2 class ="title">Ashley Walruson</h2>
            <ul id = "nav_list">
                <li><a href = "homepage.php">Home</a></li>
                <li><a href = "yourprofile.php">Profile</a></li>
                <li><a href = "messages.php">Messages</a></li>
                <li><a href = "beach.php">Beach</a></li>
                <li><a href = "about.php">About</a></li>
                <li><a href = "contact.php">Contact Us</a></li>
                <li><a href="logout.php?logout=true">Log Out</a></li>
            </ul>
        </div>
    </div>
<div class="clearfix"></div>
<div class = "container" id = "dataholder">