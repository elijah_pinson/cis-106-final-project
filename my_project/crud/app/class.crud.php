<?php

class crud
{
	
	private $db;
	
	public function __construct()
	{
		$database = new Database();
		$dbConnection = $database->dbConnection();
		$this->db = $dbConnection;
    }
	
	public function create_user($uname, $umail, $udesc)
	{
		try
		{
			$stmt = $this->db->prepare("INSERT INTO users(user_name, user_email, user_description, date_joined) VALUES(:uname, :umail, :udesc, NOW())");
			$stmt->bindparam(":uname",$uname);
			$stmt->bindparam(":umail",$umail);
			$stmt->bindparam(":udesc",$udesc);
			$stmt->execute();
			return true;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
		
	}
	public function create_article($ntitle, $nlink)
	{
		try
		{
			$stmt = $this->db->prepare("INSERT INTO news(news_title, news_link) VALUES(:ntitle, :nlink)");
			$stmt->bindparam(":ntitle",$ntitle);
			$stmt->bindparam(":nlink",$nlink);
			$stmt->execute();
			return true;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
		
	}
	
	public function getID($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM users WHERE user_id=:id");
		$stmt->execute(array(":id"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}
	
	public function getnID($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM news WHERE id=:id");
		$stmt->execute(array(":id"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}
	
	public function update($id,$uname,$umail,$udesc,$upic)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE users SET user_name=:uname, 
		                                               user_email=:umail, 
													   user_description=:udesc, 
													   user_picture=:upic
													WHERE user_id=:id ");
			$stmt->bindparam(":uname",$uname);
			$stmt->bindparam(":id",$id);
			$stmt->bindparam(":umail",$umail);
			$stmt->bindparam(":udesc",$udesc);
			$stmt->bindparam(":upic",$upic);
			$stmt->execute();
			
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	public function updatenews($id, $ntitle, $nlink)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE news SET news_title=:ntitle, 
		                                               news_link=:nlink 
													WHERE id=:id ");
			$stmt->bindparam(":ntitle",$ntitle);
			$stmt->bindparam(":id",$id);
			$stmt->bindparam(":nlink",$nlink);

			$stmt->execute();
			
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	public function delete($id, $type)
	{
		if($type == "user")
		{
			$stmt = $this->db->prepare("DELETE FROM users WHERE user_id=:id");
			$stmt->bindparam(":id",$id);
			$stmt->execute();
			return true;
		}
		else if($type == "news") 
		{
			$stmt = $this->db->prepare("DELETE FROM news WHERE id=:id");
			$stmt->bindparam(":id",$id);
			$stmt->execute();
			return true;
		}
	}
	

	

		/* paging */
	
	public function userview($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	
		if($stmt->rowCount()>0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				?>
                <tr>
                <td><?php print($row['user_id']); ?></td>
                <td><?php print($row['user_name']); ?></td>
                <td><?php print($row['user_email']); ?></td>
                <td><?php print($row['date_joined']); ?></td>
                <td><?php print($row['user_description']); ?></td>
                <td><?php print($row['user_picture']); ?></td>
                <td><?php print($row['user_online']); ?></td>
                <td align="center">
                <a href="edit-user.php?edit_id=<?php print($row['user_id']); ?>"><i class="glyphicon glyphicon-edit"></i></a>
                </td>
                <td align="center">
                <a href="deleteuser.php?delete_id=<?php print($row['user_id']); ?>"><i class="glyphicon glyphicon-remove-circle"></i></a>
                </td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}
		
	}
	
	public function newsview($query)
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
	
		if($stmt->rowCount()>0)
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				?>
                <tr>
                <td><?php print($row['id']); ?></td>
                <td><?php print($row['news_title']); ?></td>
                <td><?php print($row['news_link']); ?></td>
                <td align="center">
                <a href="edit-news.php?edit_id=<?php print($row['id']); ?>"><i class="glyphicon glyphicon-edit"></i></a>
                </td>
                <td align="center">
                <a href="deletenews.php?delete_id=<?php print($row['id']); ?>"><i class="glyphicon glyphicon-remove-circle"></i></a>
                </td>
                </tr>
                <?php
			}
		}
		else
		{
			?>
            <tr>
            <td>Nothing here...</td>
            </tr>
            <?php
		}
		
	}
	
	public function paging($query,$records_per_page)
	{
		$starting_position=0;
		if(isset($_GET["page_no"]))
		{
			$starting_position=($_GET["page_no"]-1)*$records_per_page;
		}
		$query2=$query." limit $starting_position,$records_per_page";
		return $query2;
	}
	
	public function paginglink($query,$records_per_page)
	{
		
		$self = $_SERVER['PHP_SELF'];
		
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		
		$total_no_of_records = $stmt->rowCount();
		
		if($total_no_of_records > 0)
		{
			?><ul class="pagination"><?php
			$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
			$current_page=1;
			if(isset($_GET["page_no"]))
			{
				$current_page=$_GET["page_no"];
			}
			if($current_page!=1)
			{
				$previous =$current_page-1;
				echo "<li><a href='".$self."?page_no=1'>First</a></li>";
				echo "<li><a href='".$self."?page_no=".$previous."'>Previous</a></li>";
			}
			for($i=1;$i<=$total_no_of_pages;$i++)
			{
				if($i==$current_page)
				{
					echo "<li><a href='".$self."?page_no=".$i."' style='color:red;'>".$i."</a></li>";
				}
				else
				{
					echo "<li><a href='".$self."?page_no=".$i."'>".$i."</a></li>";
				}
			}
			if($current_page!=$total_no_of_pages)
			{
				$next=$current_page+1;
				echo "<li><a href='".$self."?page_no=".$next."'>Next</a></li>";
				echo "<li><a href='".$self."?page_no=".$total_no_of_pages."'>Last</a></li>";
			}
			?></ul><?php
		}
	}
	
	/* JSON */
	
	public function jsonview($query){
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		$json = array();
	
		if($stmt->rowCount()>0) 
		{
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
			{
				$json[] = $row;
			}
		}
		
		return json_encode($json);
		
	}
	
	
}


