<?php
require('app/dbconfig.php');
require_once("app/session.php");
include_once 'app/class.crud.php';
$crud = new crud();
?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<a href="add-news.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a>
</div>

<div class="clearfix"></div><br />

<div class="container">
    <table class="table table-bordered table-responsive">
    <tr>
        <th>#</th>
        <th>Article Title</th>
        <th>Article Link</th>
        <th colspan="2" align="center">Actions</th>
    </tr>
    <?php
        $query = "SELECT id, news_title, news_link FROM news";       
        $records_per_page=5;
        $newquery = $crud->paging($query,$records_per_page);
        $crud->newsview($newquery);
    ?>
    <tr>
        <td colspan="8" align="center">
    <div class="pagination-wrap">
            <?php $crud->paginglink($query,$records_per_page); ?>
         </div>
        </td>
    </tr>
 
</table>
<a href="json.php">JSON Export</a> 
       
</div>

<?php include_once 'footer.php'; ?>