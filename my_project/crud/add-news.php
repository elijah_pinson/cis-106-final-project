<?php
require('app/dbconfig.php');
require_once("app/session.php");
include_once 'app/class.crud.php';
$crud = new crud();

if(isset($_POST['nbtn-save']))
{
    $ntitle = $_POST['news_title'];
    $nlink = $_POST['news_link'];
    if($crud->create_article($ntitle, $nlink))
	{
	    header("Location: add-news.php?inserted");
	}
	else
	{
		header("Location: add-news.php?failure");
	}
}
?>
<?php include_once 'header.php'; ?>
<div class="clearfix"></div>

<?php
if(isset($_GET['inserted']))
{
	?>
    <div class="container">
	<div class="alert alert-info">
    <strong> Record was inserted successfully </strong> <a href="news.php">Back to News</a>!
	</div>
	</div>
    <?php
}
else if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>SORRY!</strong> ERROR while inserting record !
	</div>
	</div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

 	
	 <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Article Title</td>
            <td><input type='text' name='news_title' class='form-control'></td>
        </tr>
 
        <tr>
            <td>Hyperlink</td>
            <td><input type='text' name='news_link' class='form-control'></td>
        </tr>
 
 
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="nbtn-save">
    		<span class="glyphicon glyphicon-plus"></span> Create New Record
			</button>  
            <a href="news.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to news</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>
<?php include_once 'footer.php'; ?>