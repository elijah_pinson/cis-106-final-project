<?php
require('database/dbconfig.php');
require_once("database/session.php");
include_once 'database/class.display.php';
$display = new display();
?>
<?php include_once 'header.php'; ?>
<div class="container" id ="new_users">
    <p>Ashley Walruson was designed to help dissatisfied walruses find partners outside of their 
		existing relationship. It's founded on the notion that all walruses deserve love, regardless of their situation and 
		environment. Inspiration was drawn from the Walrus Finder (a dating site for walruses) and Ashley Madison (the human 
		counterpart to this site). Users can make create and edit their own profiles, view other profiles, and send messages.</p>
</div>
<?php include_once('footer.php');?>