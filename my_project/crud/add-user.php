<?php
require('app/dbconfig.php');
require_once("app/session.php");
include_once 'app/class.crud.php';
$crud = new crud();
if(isset($_POST['btn-save']))
{
	$uname = $_POST['user_name'];
	$umail = $_POST['user_email'];
	$udesc = $_POST['user_description'];
	
	if($crud->create_user($uname, $umail, $udesc))
	{
		header("Location: add-user.php?inserted");
	}
	else
	{
		header("Location: add-user.php?failure");
	}
	
	
}

?>
<?php include_once 'header.php'; ?>
<div class="clearfix"></div>

<?php
if(isset($_GET['inserted']))
{
	?>
    <div class="container">
	<div class="alert alert-info">
    <strong>WOW!</strong> Record was inserted successfully <a href="index.php">HOME</a>!
	</div>
	</div>
    <?php
}
else if(isset($_GET['failure']))
{
	?>
    <div class="container">
	<div class="alert alert-warning">
    <strong>SORRY!</strong> ERROR while inserting record !
	</div>
	</div>
    <?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

 	
	 <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Username</td>
            <td><input type='text' name='user_name' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Email</td>
            <td><input type='text' name='user_email' class='form-control' required></td>
        </tr>
 
        <tr>
            <td>Description</td>
            <td><input type='text' name='user_description' class='form-control'></td>
        </tr>
 
        <tr>
            <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
    		<span class="glyphicon glyphicon-plus"></span> Create New Record
			</button>  
            <a href="users.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to users</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>

<?php include_once 'footer.php'; ?>