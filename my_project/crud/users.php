<?php
require('app/dbconfig.php');
require_once("app/session.php");
include_once 'app/class.crud.php';
$crud = new crud();
?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<a href="add-user.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a>
</div>

<div class="clearfix"></div><br />

<div class="container">
    <table class="table table-bordered table-responsive">
    <tr>
        <th>#</th>
        <th>user_name</th>
        <th>user_email</th>
        <th>date_joined</th>
        <th>user_description</th>
        <th>user_picture</th>
        <th>user_online</th>
        <th colspan="2" align="center">Actions</th>
    </tr>
    <?php
        $query = "SELECT user_id, user_name, user_email, date_joined, user_description, user_picture, user_online FROM users";       
        $records_per_page=5;
        $newquery = $crud->paging($query,$records_per_page);
        $crud->userview($newquery);
    ?>
    <tr>
        <td colspan="8" align="center">
    <div class="pagination-wrap">
            <?php $crud->paginglink($query,$records_per_page); ?>
         </div>
        </td>
    </tr>
 
</table>
<a href="json.php">JSON Export</a> 
       
</div>

<?php include_once 'footer.php'; ?>