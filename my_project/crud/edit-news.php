<?php
require('app/dbconfig.php');
require_once("app/session.php");
include_once 'app/class.crud.php';
$crud = new crud();
if(isset($_POST['btn-update']))
{
	$id = $_GET['edit_id'];
	$ntitle = $_POST['news_title'];
	$nlink = $_POST['news_link'];

	
	if($crud->updatenews($id, $ntitle, $nlink))
	{
		$msg = "<div class='alert alert-info'>
				<strong>WOW!</strong> Record was updated successfully <a href='news.php'>HOME</a>!
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-warning'>
				<strong>SORRY!</strong> ERROR while updating record !
				</div>";
	}
}

if(isset($_GET['edit_id']))
{
	$id = $_GET['edit_id'];
	extract($crud->getnID($id));	
}

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
	echo $msg;
}
?>
</div>

<div class="clearfix"></div><br />

<div class="container">
	 
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>Article Title</td>
            <td><input type='text' name='news_title' class='form-control' value="<?php echo $news_title; ?>" required></td>
        </tr>
 
        <tr>
            <td>Article Link</td>
            <td><input type='text' name='news_link' class='form-control' value="<?php echo $news_link; ?>" required></td>
        </tr>
 
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
    			<span class="glyphicon glyphicon-edit"></span>  Update this Record
				</button>
                <a href="news.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </td>
        </tr>
 
    </table>
</form>
     
     
</div>

<?php include_once 'footer.php'; ?>